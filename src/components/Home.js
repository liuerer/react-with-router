import React from 'react'
import '../styles/page.css'


class Home extends React.Component{
	render() {
		return (
			<div className='description'>
				<p>This is a beautiful Home Page</p>
				<p>And the url is '/'.</p>
			</div>
		);
	}
}

export default Home;
