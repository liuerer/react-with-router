import React from 'react';
import '../styles/page.css'

class ProductDetails extends React.Component {

	render() {
		if(this.props.location.state === undefined) {
			return (
				<div className='description'>
					<p>Cannot go to product details page. Please go back to Products page</p>
				</div>
			)
		}

		const {pathname} = this.props.location;
		const {id, name, price, quantity, desc} = this.props.location.state;
		return (
			<div className='description'>
				<p>Product Details:</p>
				<ul>
					<li><p>Name: {name}</p></li>
					<li><p>Id: {id}</p></li>
					<li><p>Price: {price}</p></li>
					<li><p>Quantity: {quantity}</p></li>
					<li><p>Desc: {desc}</p></li>
					<li><p>URL: {pathname}</p></li>
				</ul>
			</div>
		)
	}
}

export default ProductDetails;
