import React from 'react'
import {Link} from "react-router-dom";
import '../styles/page.css'

class AboutUs extends React.Component{

	render() {
		return(
			<div className='description'>
				<section className='about'>
					<p>Company:ThoughtWorks Local</p>
					<p>Location:Xi'an</p>
				</section>
				<section className='about'>
					<p>For more information,please</p>
					<p>view our <Link to='/'>website</Link></p>
				</section>
			</div>
		);
	}
}

export default AboutUs;
