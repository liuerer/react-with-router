import React from 'react'
import '../styles/page.css'
import {Link} from 'react-router-dom';

class Products extends React.Component {
	constructor(props){
		super(props);
		this.state={
			data: {}
		}
		this.getGoodsListFromJson = this.getGoodsListFromJson.bind(this);
	}

	getGoodsListFromJson(){
		return Object.keys(this.state.data).map(item=>{
			const { id, name } = this.state.data[item];
			const path = {
				pathname: `/products/${id}`,
				state: this.state.data[item],
			};
			return (
				<li key={id.toString()}>
					<Link to={path}>{name}</Link>
				</li>
			);
		});
	}

	render() {
		const goodsList = this.getGoodsListFromJson();
		return (
			<div className='description'>
				<p className='products'>All Products</p>
				<ul className='product-name'>{goodsList}</ul>
			</div>
		);
	}

	componentDidMount() {
		fetch('./data.json')
			.then(response => response.json())
			.then(jsonData => {
				this.setState({
					data: jsonData
				});
		})
	}
}

export default Products;
