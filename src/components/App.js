import React, {Component} from 'react';
import '../styles/App.css'
import {BrowserRouter as Router, NavLink, Route} from 'react-router-dom';
import Home from './Home'
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";
import Products from "./Products";
import ProductDetails from "./ProductDetails";

class App extends Component {
	render() {
		return (
			<Router>
				<div className="App">
					<header>
						<nav>
							<NavLink exact to='/' activeClassName='active'>Home</NavLink>
							<NavLink exact to='/products' activeClassName='active'>Products</NavLink>
							<NavLink exact to='/my-profile' activeClassName='active'>My Profile</NavLink>
							<NavLink exact to='/about-us' activeClassName='active'>About Us</NavLink>
						</nav>
					</header>
						<Route exact path='/' component={Home}/>
						<Route exact path='/products' component={Products}/>
						<Route exact path='/my-profile' component={MyProfile}/>
						<Route exact path='/about-us' component={AboutUs}/>
						<Route exact path='/products/:id' component={ProductDetails}/>
				</div>
			</Router>
		);
	}
}

export default App;
